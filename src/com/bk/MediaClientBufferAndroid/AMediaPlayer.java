package com.bk.MediaClientBufferAndroid;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class AMediaPlayer extends Activity {
    /** Called when the activity is first created. */
	
	//
	//  Android GUI
	//
	private Handler handler;
	private Button playButton;
	private Button setupButton;
	private Button pauseButton;
	private Button teardownButton;
	//private Button fourxplaybutton;
	
	// 이미지 출력을 위한 ImageView
	private ImageView imageView;
	
	private OnClickListener setupClickListener;
	private OnClickListener playClickListener;
	private OnClickListener pauseClickListener;
	private OnClickListener teardownClickListener;

	// RTP variables:
	// ----------------
	DatagramPacket rcvdp; // UDP packet received from the server
	DatagramSocket RTPsocket; // socket to be used to send and receive UDP
								// packets
	static int RTP_RCV_PORT = 25000; // port where the client will receive
										// the RTP packets

	byte[] buf; // buffer used to store data received from the server

	// RTSP variables
	// ----------------
	// rtsp states
	final static int INIT = 0;
	final static int READY = 1;
	final static int PLAYING = 2;
	static int state; // RTSP state == INIT or READY or PLAYING
	Socket RTSPsocket; // socket used to send/receive RTSP messages
	// input and output stream filters
	static BufferedReader RTSPBufferedReader;
	static BufferedWriter RTSPBufferedWriter;
	static String VideoFileName; // video file to request to the server
	int RTSPSeqNb = 0; // Sequence number of RTSP messages within the session
	int RTSPid = 0; // ID of the RTSP session (given by the RTSP Server)

	final static String CRLF = "\r\n";

	// Video constants:
	// ------------------
	static int MJPEG_TYPE = 26; // RTP payload type for MJPEG video
	
	// 
	//  added for thread solution
	//
	// Playback Buffer variables
	//
	//
	Queue<PlaybackBufEntry> pbuf = new LinkedList<PlaybackBufEntry>();
	
	//
	//Threads for
	//	1) receive frames and put into the buffer
	//	2) playback frames from the buffer at their playback time
	//
	FrameReceiver receiver = null;
	Player	player = null;

	static int FRAME_PERIOD = 100;
	static int PLAYBACK_POINT = 3*1000; // right now, simply after 3 sec 
	//Timer pbtimer;  // playback timer -- schedule tasks 
	
	long startTime, playbackTime;
	int startTS = 1; // start TimeStamp
	int latestTS = startTS; // TS of the latest frame
	private int RTSP_server_port;
	private String serverHost;
	
	private Bitmap lastestImage;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setupClickListener = playClickListener 
        	= pauseClickListener = teardownClickListener = null;

		loadLayouts();
		createClickListeners();
        setClickListeners();
        
        // 메시지를 전달받았을 때 핸들러를 실행하는 부분
		handler = new Handler() {
			public void handleMessage(Message msg) {
				lastestImage = (Bitmap) msg.obj;
				
				// ImageView 인 imgView에 그림을 출력한다.
				imageView.setImageBitmap(lastestImage);
			}
		};

		buf = new byte[15000];

        AlertDialog.Builder dialog;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.connection_dialog, null);
        
        final EditText ipEditText = (EditText) v.findViewById(R.id.ipAddress);
        final EditText portEditText = (EditText) v.findViewById(R.id.portNumber);
        final EditText fileNameEditText = (EditText) v.findViewById(R.id.fileName);
        setDefaultSetting(ipEditText, portEditText, fileNameEditText);
        
        dialog = new AlertDialog.Builder(this);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
		        if( keyCode == KeyEvent.KEYCODE_BACK ) {
                    System.exit(0);
		        	return true;
		        } else {
		            return false;
		        }
			}
		});
        dialog.setIcon(R.drawable.ic_dialog_menu_generic).setTitle("접속 설정")
                .setView(v).setCancelable(true).setPositiveButton("확인",
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                	serverHost = ipEditText.getText().toString();
                	RTSP_server_port = Integer.parseInt(portEditText.getText().toString());
                	VideoFileName = fileNameEditText.getText().toString();

					try {
	        			InetAddress ServerIPAddr;
							ServerIPAddr = InetAddress.getByName(serverHost);
	        			RTSPsocket = new Socket(ServerIPAddr, RTSP_server_port);
	        	
	        			// Set input and output stream filters:
	        			RTSPBufferedReader = new BufferedReader(new InputStreamReader(
	        					RTSPsocket.getInputStream()));
	        			RTSPBufferedWriter = new BufferedWriter(new OutputStreamWriter(
	        					RTSPsocket.getOutputStream()));
	        	
	        			// init RTSP state:
	        			state = INIT;
					} catch (Exception e) {
						e.printStackTrace();
					}
        			
                    dialog.cancel();
                }
            }).setNegativeButton("프로그램 종료",
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    System.exit(0);
                }
            }).create().show();
    }

	private void setDefaultSetting(final EditText ipEditText,
			final EditText portEditText, final EditText fileNameEditText) {
		WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipInNumber = wifiInfo.getIpAddress();
        String ipAddress = String.format("%d.%d.%d.",
          (ipInNumber & 0xff),
          (ipInNumber >> 8 & 0xff),
          (ipInNumber >> 16 & 0xff));
        
        ipEditText.setText(ipAddress);
        portEditText.setText("6666");
        fileNameEditText.setText("movie.Mjpeg");
	}
	
	private void createClickListeners() {
		setupClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
	
				System.out.println("Setup Button pressed !");
	
				if (state == INIT) {
					// Init non-blocking RTPsocket that will be used to receive data
					try {
						// construct a new DatagramSocket to receive RTP packets
						// from the server, on port RTP_RCV_PORT
						RTPsocket = new DatagramSocket(RTP_RCV_PORT);
	
						// set TimeOut value of the socket to 5msec.
						RTPsocket.setSoTimeout(5);
					} catch (SocketException se) {
						System.out.println("Socket exception: " + se);
						System.exit(0);
					}
	
					// init RTSP sequence number
					RTSPSeqNb = 1;
	
					// Send SETUP message to the server
					send_RTSP_request("SETUP");
	
					// Wait for the response
					if (parse_server_response() != 200)
						System.out.println("Invalid Server Response");
					else {
						// change RTSP state and print new state
						state = READY;
						System.out.println("New RTSP state: READY");
					}
				}// else if state != INIT then do nothing
			}
		};
	
		playClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
	
				System.out.println("Play Button pressed !");
	
				if (state == READY) {
					// increase RTSP sequence number
					RTSPSeqNb++;
	
					// Send PLAY message to the server
					send_RTSP_request("PLAY");
	
					// Wait for the response
					if (parse_server_response() != 200)
						System.out.println("Invalid Server Response");
					else {
						// change RTSP state and print out new state
						state = PLAYING;
						System.out.println("New RTSP state: PLAYING");
						
						// start the timer
						//timer.start();
						if( receiver == null && player == null ) {
							receiver = new FrameReceiver();
							player = new Player();
	
							// set 'startTime' and 'startTS' 
							startTime = System.currentTimeMillis();
							startTS = 1;
							
							receiver.start();
							player.start();
						} else {
							receiver.setResume();
							player.setResume();
						}
					}
				}// else if state != READY then do nothing
			}
		};
	
		pauseClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
	
				System.out.println("Pause Button pressed !");
	
				if (state == PLAYING) {
					// increase RTSP sequence number
					RTSPSeqNb++;
	
					// Send PAUSE message to the server
					send_RTSP_request("PAUSE");
	
					// Wait for the response
					if (parse_server_response() != 200)
						System.out.println("Invalid Server Response");
					else {
						// change RTSP state and print out new state
						state = READY;
						System.out.println("New RTSP state: READY");
	
						// stop the timer
						//timer.stop();
	
						receiver.setPause();
						player.setPause();
					}
				}
				// else if state != PLAYING then do nothing
			}
		};
	
		teardownClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
	
				System.out.println("Teardown Button pressed !");
	
				// increase RTSP sequence number
				RTSPSeqNb++;
	
				// Send TEARDOWN message to the server
				send_RTSP_request("TEARDOWN");
	
				// Wait for the response
				if (parse_server_response() != 200)
					System.out.println("Invalid Server Response");
				else {
					// change RTSP state and print out new state
					state = INIT;
					System.out.println("New RTSP state: INIT");
	
					// stop the timer
					//timer.stop();
	
					// exit
					System.exit(0);
				}
			}
		};
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		
		loadLayouts();
        setClickListeners();
        
		// ImageView 인 imgView에 그림을 출력한다.
		imageView.setImageBitmap(lastestImage);
	}

	private void setClickListeners() {		
		setupButton.setOnClickListener(setupClickListener);
		playButton.setOnClickListener(playClickListener);
		pauseButton.setOnClickListener(pauseClickListener);
		teardownButton.setOnClickListener(teardownClickListener);
	}

	private void loadLayouts() {
		setContentView(R.layout.main);

		setupButton = (Button) findViewById(R.id.setupbutton);
		playButton = (Button) findViewById(R.id.playbutton);
		pauseButton = (Button) findViewById(R.id.pausebutton);
		teardownButton = (Button) findViewById(R.id.teardownbutton);
		imageView = (ImageView) findViewById(R.id.imgview);
	}

	private int parse_server_response() {
		int reply_code = 0;

		try {
			// parse status line and extract the reply_code:
			String StatusLine = RTSPBufferedReader.readLine();
			// System.out.println("RTSP Client - Received from Server:");
			System.out.println(StatusLine);

			StringTokenizer tokens = new StringTokenizer(StatusLine);
			tokens.nextToken(); // skip over the RTSP version
			reply_code = Integer.parseInt(tokens.nextToken());

			// if reply code is OK get and print the 2 other lines
			if (reply_code == 200) {
				String SeqNumLine = RTSPBufferedReader.readLine();
				System.out.println(SeqNumLine);

				String SessionLine = RTSPBufferedReader.readLine();
				System.out.println(SessionLine);

				// if state == INIT gets the Session Id from the SessionLine
				tokens = new StringTokenizer(SessionLine);
				tokens.nextToken(); // skip over the Session:
				RTSPid = Integer.parseInt(tokens.nextToken());
			}
		} catch (Exception ex) {
			System.out.println("Exception caught: " + ex);
			System.exit(0);
		}

		return (reply_code);
	}
    
    private void send_RTSP_request(String request_type) {
		try {
			// Use the RTSPBufferedWriter to write to the RTSP socket

			// write the request line:
			RTSPBufferedWriter.write(request_type + " " + VideoFileName + " RTSP/1.0" + CRLF);

			// write the CSeq line:
			RTSPBufferedWriter.write("CSeq: " + RTSPSeqNb + CRLF);

			// check if request_type is equal to "SETUP" and in this case write
			// the Transport: line advertising to the server the port used to
			// receive the RTP packets RTP_RCV_PORT
			// otherwise, write the Session line from the RTSPid field
			if( request_type.equals("SETUP") ) {
			    RTSPBufferedWriter.write("Transport: RTP/UDP; client_port= " + RTP_RCV_PORT + CRLF);
			} else {
			    RTSPBufferedWriter.write("Session: " + RTSPid + CRLF);
			}

			RTSPBufferedWriter.flush();
		} catch (Exception ex) {
			System.out.println("Exception caught: " + ex);
			System.exit(0);
		}
	}
    
    /* 
     * Playback Buffer Entry
     *   - internal class for A Media Player 
     */
    
    class PlaybackBufEntry {
    	private int length;
    	private byte[] buf;	
    	private int	timestamp;

    	public PlaybackBufEntry(int length, byte[] buf, int ts) {  
    		this.length = length;
    		this.buf = buf.clone();	
    		this.timestamp = ts;
    	}
    	
    	public int getLength() {
    		return length;
    	}
    	
    	public byte[] getBuffer() {
    		return buf;
    	}
    	
    	public int getTimeStamp() {
    		return timestamp;
    	}
    }

    /* 
     * FrameReceiver
     *   - internal class for A Media Player
     *   - receive a frame and put it into the playback buffer
     */

    class FrameReceiver extends Thread {
    	private boolean PLAY_ON;
    	private DatagramPacket rcvdp; // UDP packet received from the server
    	
    	public FrameReceiver() {  
    		this.PLAY_ON = true;
    	}

    	public void run() {
    		while (true) {    			
    			synchronized (this) {
    				if (!PLAY_ON)
    					try {
    						wait();
    					} catch (InterruptedException e1) {
    						// TODO Auto-generated catch block
    						e1.printStackTrace();
    					}							
    			}
    			
    			// receive a UDP datagram -- may be blocked here
				try {
					rcvdp = new DatagramPacket(buf, buf.length);
					RTPsocket.receive(rcvdp);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					continue;
				}
    			
    			// create an RTPpacket object from the Datagram 
				RTPpacket rtp_packet = new RTPpacket(rcvdp.getData(), rcvdp
				.getLength());
    			
    			// print important header fields of the RTP packet received:
				System.out.println("Got RTP packet with SeqNum # "
				+ rtp_packet.getsequencenumber() + " TimeStamp "
				+ rtp_packet.gettimestamp() + " ms, of type "
				+ rtp_packet.getpayloadtype());

				// 이전과 동일하게 추가 print header bitstream:
				rtp_packet.printheader();

				// 이전과 동일하게 추가 get the payload bitstream from the RTPpacket object
				int payload_length = rtp_packet.getpayload_length();
				byte[] payload = new byte[payload_length];
				rtp_packet.getpayload(payload);
 			
    			// enqueue the frame in the RTPpacket object				

    			PlaybackBufEntry be = new PlaybackBufEntry(payload_length, payload, rtp_packet.gettimestamp());    			
    			pbuf.offer(be);	
    		}
    	}	
    	
    	public void setPause() {
    		synchronized (this) {
    			PLAY_ON = false;
    		}
    	}
    	
    	public void setResume() {
    		synchronized (this) {
    			PLAY_ON = true;
        		notifyAll();
    		}
    	}
    }

    /* 
     * Player
     *   - internal class for A Media Player
     *   - receive a frame and put it into the playback buffer
     */

    class Player extends Thread {
    	private boolean PLAY_ON;    	
    	private byte[] buf; // buf for image

    	private int playCounter = 0;
		private long pausedTime;

    	public Player() {		
    		this.PLAY_ON = true;
    	} 
    	
    	public void run() {
    		latestTS = 0;
    		while (true) {
    			synchronized (this) {
    				if (!PLAY_ON)
    					try {
    						wait();
    					} catch (InterruptedException e1) {
    						// TODO Auto-generated catch block
    						e1.printStackTrace();
    					}							
    			}
    			
    			try {
    				PlaybackBufEntry be = pbuf.remove(); // dequeue a buffer entry which has a frame with TS
    				
    				// calculate the playback time of the frame
    				startTS = be.getTimeStamp();
    				playCounter += startTS - latestTS;
    				latestTS = startTS;
    				playbackTime = startTime + PLAYBACK_POINT + playCounter;
    				
    				// decide how long it should wait until the playback time
    		    	long pdelay = playbackTime - System.currentTimeMillis();
    		    	
    		    	if (pdelay>0) { 
    		    		try {
    		    			synchronized (this) {
    							wait(pdelay);
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
    		    		
    		    		// get an Image object from the buffer entry
    		    		buf = be.getBuffer();

    					// display the image through message handler
    		    		buf = be.buf;
        				Bitmap img = BitmapFactory.decodeByteArray(buf, 0, buf.length);
        				Message msg = handler.obtainMessage();
        				msg.obj = img;
        				handler.sendMessage(msg);
    		    	}  		    	

    				
    			} catch(NoSuchElementException e){
    				System.out.println("Stop playing due to no more frame at buffer - not necessarily error");
    				
					synchronized (this) {
						try {
			    			pausedTime = System.currentTimeMillis();
	    					wait(PLAYBACK_POINT - FRAME_PERIOD); // wake up a little earlier
	    	    			startTime += System.currentTimeMillis() - pausedTime;
						} catch (InterruptedException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
					}
    			}
    		}
    	}	
    	
    	public void setPause() {
    		synchronized (this) {
    			PLAY_ON = false;
    			pausedTime = System.currentTimeMillis();
    		}
    	}
    	
    	public void setResume() {    		
    		// set 'startTime' and 'startTS' properly 
    		synchronized (this) {
    			PLAY_ON = true;
    			startTime += System.currentTimeMillis() - pausedTime;
        		notifyAll();
    		}   		
    	}
    }
} // end of Class 'AMediaPlayer'